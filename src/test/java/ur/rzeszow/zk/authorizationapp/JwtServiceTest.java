/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp;

import org.junit.Test;
import static org.junit.Assert.*;
import ur.rzeszow.zk.authorizationapp.dto.UserTO;
import ur.rzeszow.zk.authorizationapp.repositories.User;

/**
 *
 * @author zabek
 */
public class JwtServiceTest {
    
    public JwtServiceTest() {
    }

    @Test
    public void testToken() {
        User u = new User();
        u.setAdmin(true);
        u.setId(1);
        u.setLogin("login");
        
        String token = JwtService.createToken(u);
        UserTO userFromToken = JwtService.getUserFromToken(token);
        
        assertEquals(u.getId(), userFromToken.getId().intValue());
        assertEquals(u.getLogin(), userFromToken.getName());
        assertEquals("admin", userFromToken.getRole());
    }
    
}
