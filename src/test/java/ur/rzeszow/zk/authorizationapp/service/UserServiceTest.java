/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp.service;

import java.math.BigDecimal;
import java.util.Optional;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import ur.rzeszow.zk.authorizationapp.repositories.User;
import ur.rzeszow.zk.authorizationapp.repositories.UserRepository;

/**
 *
 * @author zabek
 */
@RunWith(SpringRunner.class)
public class UserServiceTest {
    
    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @Bean
        public UserService userService() {
            return new UserService();
        }
    }
    
    @Autowired
    UserService userService;
    
    @MockBean
    private UserRepository userRepository;
    
    @Before
    public void setUp() {
        User u = new User();

        Mockito.when(userRepository.findByLogin("user")).thenReturn(Optional.empty());
        Mockito.when(userRepository.findByLogin("user2")).thenReturn(Optional.of(u));
    }
    
    @Test
    public void testLoginIsNotFree(){
        AppException exception = Assertions.assertThrows(AppException.class, () -> {
            userService.createUser("user2", "", "");
        });

        assertTrue(exception.getMessage().contains("Wybrany login jest zajęty"));
    }
    
    @Test
    public void testToShortPassword(){
        AppException exception = Assertions.assertThrows(AppException.class, () -> {
            userService.createUser("user1", "12", "");
        });

        assertTrue(exception.getMessage().contains("Hasło musi się składać przynajmniej z 5 znaków"));
    }
    
    @Test
    public void testToShortLogin(){
        AppException exception = Assertions.assertThrows(AppException.class, () -> {
            userService.createUser("user", "1212312", "");
        });

        assertTrue(exception.getMessage().contains("Login musi mieć cn 5 znaków"));
    }
}
