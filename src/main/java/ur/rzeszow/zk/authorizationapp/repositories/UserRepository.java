/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp.repositories;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author zabek
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
    public Optional<User> findByLogin(String login);
    public Optional<User> findByLoginAndPasswordHash(String login, String password);
}
