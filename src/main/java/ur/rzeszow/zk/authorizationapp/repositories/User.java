/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp.repositories;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author zabek
 */
@Entity
@Table(name="Users")
@Data
public class User implements Serializable {
    private static final long serialVersionUID = -2343243243242432341L;

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "login")
    private String login;
    
    @Column(name = "password_hash")
    private String passwordHash;
    
    @Column(name = "is_admin")
    private boolean isAdmin = false;
    
    @Column(name = "email")
    private String eamil;
    
    public String getUserRole(){
        if(isAdmin){
            return UserRoles.ADMIN;
        }
        return UserRoles.USER;
    }

}
