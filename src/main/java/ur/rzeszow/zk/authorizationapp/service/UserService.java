/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp.service;

import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ur.rzeszow.zk.authorizationapp.AuthorizationException;
import ur.rzeszow.zk.authorizationapp.dto.ChangePasswordTO;
import ur.rzeszow.zk.authorizationapp.dto.LoginTO;
import ur.rzeszow.zk.authorizationapp.repositories.User;
import ur.rzeszow.zk.authorizationapp.repositories.UserRepository;

/**
 *
 * @author zabek
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public Optional<User> findUserById(Integer id){
        return userRepository.findById(id);
    }
    
    public Integer createUser(String userName, String password, String email){
        Optional<User> existingUser = userRepository.findByLogin(userName);
        if(existingUser.isPresent()){
            throw new AppException("Wybrany login jest zajęty");
        }
        if(password == null || password.length() < 5){
            throw new AppException("Hasło musi się składać przynajmniej z 5 znaków");
        }
        if(userName == null || userName.length() < 5){
            throw new AppException("Login musi mieć cn 5 znaków");
        }
        
        User u = new User();
        u.setAdmin(false);
        u.setEamil(email);
        u.setLogin(userName);
        u.setPasswordHash(hashPassword(password));
        userRepository.save(u);
        return u.getId();
    }
     
    public User loginUser(LoginTO login){
        String hash = hashPassword(login.getPassword());
        return userRepository.findByLoginAndPasswordHash(login.getLogin(), hash)
                .orElseThrow(() -> new AuthorizationException("Błędne dane logowania"));
    }
    
    public void changePasswort(ChangePasswordTO params){
        User user = findUserById(params.getId()).orElseThrow(() -> new AppException("Nie znaleziono takiego użytkownika"));
        if(!user.getPasswordHash().equals(hashPassword(params.getOldPassword()))){
            throw new AppException("Błędne hasło");
        }
        if(params.getNewPassword().length() < 5){
            throw new AppException("Hasło musi się składać przynajmniej z 5 znaków");
        }
        
        user.setPasswordHash(hashPassword(params.getNewPassword()));
        userRepository.save(user);
    }
    
    public static String hashPassword(String psw){
        return Hashing.sha512().hashString(psw, StandardCharsets.UTF_8).toString();
    }
}
