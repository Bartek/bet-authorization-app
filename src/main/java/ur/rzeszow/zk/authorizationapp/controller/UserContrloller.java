/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.math.BigDecimal;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import ur.rzeszow.zk.authorizationapp.AuthorizationException;
import ur.rzeszow.zk.authorizationapp.JwtService;
import ur.rzeszow.zk.authorizationapp.dto.ChangePasswordTO;
import ur.rzeszow.zk.authorizationapp.dto.CreateUserTO;
import ur.rzeszow.zk.authorizationapp.dto.LoginTO;
import ur.rzeszow.zk.authorizationapp.dto.TokenTO;
import ur.rzeszow.zk.authorizationapp.dto.UserTO;
import ur.rzeszow.zk.authorizationapp.repositories.User;
import ur.rzeszow.zk.authorizationapp.service.AppException;
import ur.rzeszow.zk.authorizationapp.service.UserService;

/**
 *
 * @author zabek
 */
@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserContrloller {
    @Autowired
    UserService userService;
    
    @GetMapping()
    public UserTO getUser(HttpServletRequest req){
        String token = JwtService.resolveToken(req);
        if(token == null){
//            throw new AppException("Nie podano tokenu");
        }
        
        UserTO user = JwtService.getUserFromToken(token);
        RestTemplate rest = new RestTemplate();
        try {
            ResponseEntity<BigDecimal> exchange = rest.exchange(
                    "http://host.docker.internal:8080/bet-app/funds/user?userId=" + user.getId(),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    BigDecimal.class);
            user.setFunds(exchange.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return user;
    }
    
    @PostMapping("login")
    public ResponseEntity<TokenTO> login(@RequestBody LoginTO login){
        try {
            User u = userService.loginUser(login);
            return new ResponseEntity<>(new TokenTO(JwtService.createToken(u)), HttpStatus.OK);
        } catch (AuthorizationException e) {
            throw new AppException(e);
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    @PostMapping("create")
    public void create(@RequestBody CreateUserTO create){
        userService.createUser(create.getLogin(), create.getPassword(), create.getEmail());
    }
    
    @PostMapping("change-password")
    public void changePassword(@RequestBody ChangePasswordTO changePasswordParam){
        userService.changePasswort(changePasswordParam);
    }
    
}
