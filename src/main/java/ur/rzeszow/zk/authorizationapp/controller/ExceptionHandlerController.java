/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import ur.rzeszow.zk.authorizationapp.service.AppException;

/**
 *
 * @author zabek
 */
@ControllerAdvice
public class ExceptionHandlerController {

    @org.springframework.web.bind.annotation.ExceptionHandler(AppException.class)
    public ResponseEntity<String> BetError(HttpServletRequest req, Exception ex) {
        ex.printStackTrace();
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleError(HttpServletRequest req, Exception ex) {
        ex.printStackTrace();
        return new ResponseEntity<>("Wystąpił nieoczekiwany błąd, spróbuj ponownie później.", HttpStatus.BAD_REQUEST);
    }
}
