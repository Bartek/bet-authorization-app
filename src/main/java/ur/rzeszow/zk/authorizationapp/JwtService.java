/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import ur.rzeszow.zk.authorizationapp.dto.UserTO;
import ur.rzeszow.zk.authorizationapp.repositories.User;

/**
 *
 * @author zabek
 */
public class JwtService {
    public static final String KEY = "asdfghad1sadasf13";
    
    public static String createToken(User u) {
        try {
            return Jwts.builder()
                    .setSubject(u.getLogin())
                    .setId(String.valueOf(u.getId()))
                    .claim("role", u.getUserRole())
                    .setIssuedAt(new Date())
                    .setExpiration(new Date(System.currentTimeMillis() + 100000000))
                    .signWith(
                            SignatureAlgorithm.HS256,
                            KEY.getBytes("UTF-8")
                    )
                    .compact();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(JwtService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public static UserTO getUserFromToken(String token){
        Claims claims;
        try {
            token = token.replace("Bearer ", "");
            claims = Jwts.parser().setSigningKey(KEY.getBytes("UTF-8")).parseClaimsJws(token).getBody();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(JwtService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        UserTO user = new UserTO();
        user.setName(claims.getSubject());
        user.setId(Integer.valueOf(claims.getId()));
        user.setRole(claims.get("role", String.class));
        return user;
    }
    
    public static String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        return bearerToken;
//        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
//            return bearerToken.substring(7, bearerToken.length());
//        }
//        return null;
    }
}
