/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.authorizationapp.dto;

import java.math.BigDecimal;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author zabek
 */
@Data
@NoArgsConstructor
public class UserTO {
    private Integer id;
    private String name;
    private BigDecimal funds;
    private String role;
    private String email;
    private String token;
}
