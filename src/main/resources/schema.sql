create table users(
    id serial not null primary key,
    login text not null,
    password_hash text not null,
    is_admin bool not null,
    email text not null
);